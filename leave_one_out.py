#!/usr/bin/env python

import argparse
from keras.models import load_model
import sklearn.model_selection
from util import load_from_saved
import numpy as np
import math


parser = argparse.ArgumentParser()
parser.add_argument('--snp-file', help="File containing snps", default='./wgs681.snp.vcf')
parser.add_argument('--phen-file', help="File containing phenotypes", default='./N.G-MICs.xlsx')
parser.add_argument('--phenotype', help="Which phenotype are you testing", default='cefixime',
                    choices=['cefixime', 'azithromycin', 'ciprofloxacin'])
parser.add_argument('--model-type', help="Classification or regression", choices=['classification', 'regression'])
parser.add_argument('--data-type', help="Binary or uncertain", choices=['binary', 'uncertain'])
parser. add_argument('--model-file', help="Model File")
args = parser.parse_args()


if 'wgs681' in args.snp_file:
    import gon_loader as loader
else:
    import lentil_loader as loader

@load_from_saved('%s.pickle' % args.snp_file)
def load_snps():
    return loader.load_snps(args.snp_file, args.data_type)

@load_from_saved('%s.%s.pickle' % (args.snp_file, args.data_type))
def convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts):
    return loader.convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts, args.data_type)

@load_from_saved('%s.%s.pickle' % (args.phenotype, args.model_type))
def get_phenotypes(sample_ids, data):
    return loader.get_phenotypes(sample_ids, args.phen_file, args.phenotype, args.model_type, data)

data, snp_ids, sample_ids, n_gts = load_snps()
data, sample_ids = convert_snps_to_np(data, snp_ids, sample_ids, n_gts)
y, data = get_phenotypes(sample_ids, data)
print(data.shape)

# make this run faster
x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(
        data, y, test_size=0.2, random_state=234)
data = x_test
y = y_test

model = load_model(args.model_file)

base_loss, base_accuracy = model.evaluate(data, y)
print(base_loss, base_accuracy)

# data shape = (676, 41502, 1, 7)
n_samples, n_snps, _, input_vector_size = data.shape

print("input_vector_size", input_vector_size, "snps", n_snps)

if 'lentil' in args.snp_file:
    # TODO move into lentil loader
    snp_ids.extend(['year', 'field'])

assert n_snps == len(snp_ids), "n_snps %d, snp_ids %d" % (n_snps, len(snp_ids))
snp_scores = []

for idx, snp_id in enumerate(snp_ids):
    _data = data.copy()
    # data[:, idx, :, :].shape -> n_samples, 1, input_vector_size
    # so make a mask of zeros with the same shape
    mask = np.zeros(input_vector_size).repeat(n_samples).reshape(n_samples, 1, input_vector_size)
    _data[:, idx, :,  :] = mask
    loss, accuracy = model.evaluate(_data, y)
    print(loss, accuracy)
    snp_scores.append({'loss': loss,
                       'accuracy': accuracy,
                       'snp_id': snp_id,
                       'loss_difference': math.fabs(loss - base_loss),
                       'accuracy_difference': math.fabs(accuracy - base_accuracy)})

print('loss sort')
print(sorted(snp_scores, reverse=True, key=lambda x: x['loss_difference'])[0:10])
print('accuracy sort')
print(sorted(snp_scores, reverse=True, key=lambda x: x['accuracy_difference'])[0:10])




