#!/usr/bin/env python
import argparse
import collections
import difflib
import itertools
import keras
import math
import numpy as np
import pandas as pd
import pickle
import sklearn
import sklearn.metrics
import sklearn.model_selection
import sklearn.preprocessing
import datetime
import sys
from functools import wraps
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from keras.models import Sequential, load_model
from keras import regularizers, callbacks, backend


from util import load_from_saved

gon_pheno_choices = ['cefixime', 'azithromycin', 'ciprofloxacin', 'tetracycline', 'penicillin']
parser = argparse.ArgumentParser()
parser.add_argument('--snp-file', help="File containing snps", default='./wgs681.snp.vcf')
parser.add_argument('--phen-file', help="File containing phenotypes", default='./N.G-MICs.xlsx')
parser.add_argument('--phenotype-predict', help="Which phenotype are you testing", default='cefixime',
                    choices=gon_pheno_choices)
parser.add_argument('--phenotype-transfer', help="Which phenotype are you transferring from", default='cefixime',
                    choices=gon_pheno_choices)
parser.add_argument('--model-type', help="Classification or regression", choices=['classification', 'regression'])
parser.add_argument('--data-type', help="Binary or uncertain", choices=['binary', 'uncertain'])
args = parser.parse_args()
print(args)

if args.phenotype_predict == args.phenotype_transfer:
    sys.exit(0)
pheno = args.phenotype_predict



if 'wgs681' in args.snp_file:
    import gon_loader as loader
else:
    import lentil_loader as loader

start_timestamp = datetime.datetime.now()



@load_from_saved('%s.pickle' % args.snp_file)
def load_snps():
    return loader.load_snps(args.snp_file, args.data_type)

@load_from_saved('%s.%s.pickle' % (args.snp_file, args.data_type))
def convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts):
    return loader.convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts, args.data_type)

def get_phenotypes(sample_ids, data, phenotype, n_gts):
    print("retrieving phenotype %s from %s" % (phenotype, args.phen_file))
    return loader.get_phenotypes(sample_ids, args.phen_file, phenotype, args.model_type, data, n_gts)


def explained_variance_fn(x_test, y_test, model):
    def explained_variance(_y_true, _y_pred):
        y_pred = model.predict(x_test)
        return backend.variable(sklearn.metrics.explained_variance_score(y_test, y_pred))
    return explained_variance
def r2_fn(x_test, y_test, model):
    def r2(_y_true, _y_pred):
        y_pred = model.predict(x_test)
        return backend.variable(sklearn.metrics.r2_score(y_test, y_pred))
    return r2

data, snp_ids, sample_ids, n_gts = load_snps()
print('n_gts:', n_gts)
data, sample_ids = convert_snps_to_np(data, snp_ids, sample_ids, n_gts)



y, data = get_phenotypes(sample_ids, data, pheno, n_gts)
n_snps, n_samples, d, input_vector_size = data.shape
print(data.shape)


# remove nan values
missing_phenos = np.argwhere(np.isnan(y))
print(len(missing_phenos), "missing phenotypes")
for idx in missing_phenos:
    data = np.delete(data, idx[0], 0)
y = y[~np.isnan(y)]

x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(
    data, y, test_size=0.1, random_state=123)

print('*'*80)
print('Transfer learning from %s to %s' % (args.phenotype_transfer, pheno))

# load model
model_file = '%s.%s.%s.%s.h5' % (args.snp_file, args.phenotype_transfer, args.model_type, args.data_type)
model = load_model(model_file)
for layer in model.layers[:9]:
    layer.trainable = False
model.compile(loss='mse',
              optimizer='adam',
              metrics=['accuracy'])
model.summary()

model.fit(x_train, y_train, epochs=200, batch_size=128, validation_data=(x_test, y_test), callbacks=[
    callbacks.TerminateOnNaN(),
    callbacks.TensorBoard(histogram_freq=1, write_grads=True,
                          log_dir='./logs/%s.%s.%s-to-%s' % (start_timestamp, args.data_type, args.phenotype_transfer, pheno)),
    callbacks.EarlyStopping('val_loss', min_delta=1, patience=5, )
])

if args.model_type == 'regression':
    y_pred = model.predict(x_train)
    print('Training')
    print('Explained variance:', sklearn.metrics.explained_variance_score(y_train, y_pred))
    print('R2:', sklearn.metrics.r2_score(y_train, y_pred))

    y_pred = model.predict(x_test)
    print('Validation')
    print('Explained variance:', sklearn.metrics.explained_variance_score(y_test, y_pred))
    print('R2:', sklearn.metrics.r2_score(y_test, y_pred))

print(model.metrics_names)


# Without FC layers -> 73.85% accuracy
