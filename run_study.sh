#!/bin/bash
set -e

## Gonorrhoeae version
#for x in 4
#do
#for args in {azithromycin,cefixime,ciprofloxacin,penicillin,tetracycline}+classification+{binary,uncertain}
#do
#    echo run $x
#    read  from model data < <(sed 's/\+/ /g' <<< $args)
#    mkdir -p logan-study-${x}
#
#    ./study.py  --snp-file ./wgs681.snp.vcf --phen-file ./N.G-MICs.xlsx --phenotype $from --model-type $model --data-type $data | tee logan-study-${x}/${from}.${model}.${data}.out
#done

# Lentil version
for args in {0,1,2}+regression+{uncertain,binary}
do
    read  pheno model data < <(sed 's/\+/ /g' <<< $args)
    mkdir -p all-lentil-3

    ./study.py --snp-file /u1/ljk003/lentil-filtered-100.vcf.recode.vcf --phen-file /u1/ljk003/LDP_DataForLogan.xlsx --phenotype $pheno --model-type $model --data-type $data | tee all-lentil-3/${pheno}.${model}.${data}.out
done
