
# Download and validate
prefetch $(<accessions.txt) | vdb-validate ./ | fastq-dump --split-files $(<accessions.txt)

# Align short reads to reference
bwa mem -R "@RG\tID:$sra\tSM:US/UK/CA-$sra" NCCP11945_NG.fasta $sra"_1.fastq" $sra"_2.fastq" -o $sra".sam"
  

# convert sam files to bam and sort
samtools view -buh $sra".sam" | samtools sort -o $sra".sorted.bam"


# SNP calling with freebayes
freebayes --ploidy 1 --min-mapping-quality 30 --min-base-quality 30 --min-alternate-fraction 0.75 --min-coverage 15 \
 	-f NCCP11945_NG.fasta --bam-list wgs681.bam.txt > wgs681.vcf


# filter out low quality SNPs and indels
vcffilter -f "DP > 4" -f "QUAL > 20" wgs681.vcf > wgs681.filtered.vcf
vcffilter -f "TYPE = snp" wgs681.filtered.vcf > wgs681.snp.vcf



