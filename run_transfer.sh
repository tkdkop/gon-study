#!/bin/bash
set -e

for args in {tetracycline,penicillin}+{ciprofloxacin,tetracycline,penicillin}+regression+uncertain
do
    read pheno1 pheno2 model data < <(sed 's/\+/ /g' <<< $args)
    mkdir -p transfer-results

    ./transfer.py  --snp-file ./wgs681.snp.vcf --phen-file ./N.G-MICs.xlsx --phenotype-transfer $pheno1 --phenotype-predict $pheno2 --model-type $model --data-type $data | tee transfer-results-2/${pheno1}-${pheno2}.${model}.${data}.out 

done
