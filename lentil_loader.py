from util import load_from_saved
import pandas as pd
import collections
import vcf
import numpy as np


def load_snps(snp_file, data_type):
    snp_file = snp_file
    data = []
    sample_ids = []
    snp_ids = []
    max_alt = 1
    with open(snp_file, 'r') as f:
        vcf_reader = vcf.Reader(f)
        for record in iter(vcf_reader):
        #for record in itertools.islice(vcf_reader, 100,105):
            _data = collections.defaultdict(list)
            snp_ids.append((record.CHROM, record.POS))
            save_sample_name = False
            if not sample_ids:
                # save all sample id's from the first SNP record
                save_sample_name = True

            for sample in record.samples:
            #for sample in [record.samples[0]]:
                #print(sample, sample.data, sample.data.GT)
                if save_sample_name:
                    sample_ids.append(sample.sample)
                print(sample.data)

                pl = sample.data.PL

                if sample.data.GT != './.':
                    if len(pl) > max_alt:
                        #assert False
                        max_alt = len(pl)
                    if data_type == 'uncertain':
                        idx = pl.index(0)
                        pl = [10**(-float(x) / 10) for x in pl]
                        quality = max(sample.data.PL)
                        pl[idx] = 1 - 10**(-float(quality) / 10)	
                      
                    else:
                        idx = pl.index(0)
                        pl = [0] * len(pl)
                        pl[idx] = 1

                else:
                    pl = [0]

                print(pl)
                _data[sample.sample] = pl
            data.append(_data)
    print(len(sample_ids), len(snp_ids))
    return data, snp_ids, sample_ids, max_alt

def convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts, data_type):
    iids = ['Nepal1', 'Nepal2', 'Nepal3', 'Rosthern1', 'Rosthern2', 'Rosthern3', 'Sutherland1', 'Sutherland2', 'Sutherland3']
    print(n_gts)
    # data is of the form:
    # list:SNP(dict:Sample,call)
    # [ { A:A1, B:B1, C:C1 }
    #   { A:A2, B:B2, C:C2 }
    #   { A:A3, B:B3, C:C3 } ]
    # it needs to be in the form:
    # [ [ A1 A2 A3 ]
    #   [ B1 B2 B3 ]
    #   [ C1 C2 C3 ] ]
    # Where letters are the sample identifiers, and numbers are the snp identifiers
    # Final size should be (453, 9, 20811, 1) after 1-hot encoding
    missing_calls = 0
    all_sample_ids = []
    #data = np.zeros(shape=(len(sample_ids)*len(iids)*2, n_gts, len(snp_ids)+2, 1))
    data = np.zeros(shape=(len(sample_ids), n_gts, len(snp_ids), 1))
    print(data.shape)
    for i, sample_id in enumerate(sample_ids):
		#for y_id, year in enumerate((2016, 2017)):
		#    for f_id, field in enumerate(iids):
                #sample_name = sample_id + ':' + str(year) + field
                sample_name = sample_id
                all_sample_ids.append(sample_name)
                for j, snp_id in enumerate(snp_ids):
                    gl = snp_dict[j][sample_id]

                    _g = np.zeros(n_gts)
                    for k,gt in enumerate(gl):
                        _g[k] = gt

                    for k,gt in enumerate(_g):
                        data[i][k][j][0] = gt
                #data[i][y_id][j+1][0] = 1
                #data[i][f_id][j+2][0] = 1

    print(data)

    print('missed', missing_calls, missing_calls/(len(snp_ids)*len(sample_ids))*100, '%')
    print(data.shape)
    return data, all_sample_ids

def get_phenotypes(sample_ids, phen_file, phenotype=None, model_type=None, gen_data=None):
    phenotype_list = [
        'Days till 10% of Plants have One Open Flower (R1; days)',
        'Days till 10% of Plants have fully Swollen Pods (R5; days)',
        'Days till 10% of Plants have 1/2 Pods Mature (R7; days)'
    ]
    print(gen_data.shape)
    # (324, 10, 4314563, 1)
    gen_data = np.reshape(gen_data, (324, -1,1,10))
    df = pd.read_excel(phen_file, sheet_name='AvgData2', header=0)
    data = {}
    sample_ids = [str(i).strip() for i in sample_ids]

    for i, row in df.iterrows():
        if row['Expt'] != 'Sutherland 2016':
            continue
        sample_name = str(row['Entry']).strip()
        if str(sample_name) not in sample_ids:
            print(sample_name, 'not in data, skipping')
        mic = row[phenotype_list[int(phenotype)]]
        if mic is pd.np.nan:
            mic = 0
        data[sample_name] = mic
    a = np.array([])
    missing_count = 0
    for sample_id in reversed(sample_ids):
        try:
            a = np.insert(a, 0, data[sample_id], axis=0)
        except KeyError:
            print('Could not find', sample_id, 'in', sample_ids[:5])
            i = sample_ids.index(sample_id)
            gen_data = np.delete(gen_data, i, 0)
            missing_count += 1
    print(a.shape)
    print(gen_data.shape)
    print("skipped", missing_count, "generated samples")
    return a, gen_data
