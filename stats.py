#!/usr/bin/env python

import argparse
import vcf
import collections
import numpy as np
import seaborn
import sys
import matplotlib.pyplot as plt
import scipy.stats


parser = argparse.ArgumentParser()
parser.add_argument('--file', help='VCF file')

args = parser.parse_args()

seaborn.set_style('darkgrid')

with open(args.file, 'r') as f:
    qualities = []
    n_snps = 0
    n_samples = 0
    n_missing = 0
    n_called = 0
    missing_snps = 0
    vcf_reader = vcf.Reader(f)
    for snp in iter(vcf_reader):
        n_snps += 1
        if n_samples != len(snp.samples):
            n_samples = len(snp.samples)
            print('New number of samples', n_samples)
        for sample in snp.samples:
            if isinstance(sample.data.GQ, int):
                qualities.append(sample.data.GQ)
                n_called += 1
            else:
                n_missing += 1

print(n_snps, 'SNPs', n_samples, 'Samples')
print(n_called, 'Called', n_missing, 'missing', float(n_called)/float(n_missing + n_called), '%')

print(scipy.stats.describe(qualities))

seaborn.distplot(qualities, kde=False)
plt.show()
