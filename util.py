
from functools import wraps
import os

import pickle

def load_from_saved(pickle_file):
    def decorator(func):
        @wraps(func)
        def inner(*args, **kwargs):
            max_bytes = 2**31 - 1
            try:
                f = open(pickle_file, 'rb')
                print('!! Pickle file {} found, skipping function {}'.format(pickle_file,
                                                                             func.__name__))
                input_size = os.path.getsize(pickle_file)
                bytes_in = bytearray(0)
                for _ in range(0, input_size, max_bytes):
                    bytes_in += f.read(max_bytes)
                data = pickle.loads(bytes_in)
                f.close
            except IOError as e:
                data = func(*args, **kwargs)
                if data is not None:
                    f = open(pickle_file, 'wb')
                    bytes_out = pickle.dumps(data, protocol=4)
                    for idx in range(0, len(bytes_out), max_bytes):
                        f.write(bytes_out[idx:idx+max_bytes])
                    f.close
            return data
        return inner
    return decorator
