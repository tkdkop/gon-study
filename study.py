#!/usr/bin/env python
import argparse
import collections
import difflib
import itertools
import keras
import math
import numpy as np
import pandas as pd
import pickle
import sklearn
import sklearn.metrics
import sklearn.model_selection
import sklearn.preprocessing
import datetime
from functools import wraps
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from keras.models import Sequential
from keras import regularizers, callbacks, backend
from keras.utils import plot_model


from util import load_from_saved


parser = argparse.ArgumentParser()
parser.add_argument('--snp-file', help="File containing snps", default='./wgs681.snp.vcf')
parser.add_argument('--phen-file', help="File containing phenotypes", default='./N.G-MICs.xlsx')
parser.add_argument('--phenotype', help="Which phenotype are you testing", default='cefixime')
                    
parser.add_argument('--model-type', help="Classification or regression", choices=['classification', 'regression'])
parser.add_argument('--data-type', help="Binary or uncertain", choices=['binary', 'uncertain'])
args = parser.parse_args()
print(args)

if 'wgs681' in args.snp_file or 'gon' in args.snp_file:
    import gon_loader as loader
else:
    import lentil_loader as loader

start_timestamp = datetime.datetime.now()



@load_from_saved('%s.pickle' % args.snp_file)
def load_snps():
    return loader.load_snps(args.snp_file, args.data_type)

@load_from_saved('%s.%s.pickle' % (args.snp_file, args.data_type))
def convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts):
    return loader.convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts, args.data_type)

#@load_from_saved('%s.%s.pickle' % (args.phenotype, args.model_type))
def get_phenotypes(sample_ids, data, n_gts):
    return loader.get_phenotypes(sample_ids, args.phen_file, args.phenotype, args.model_type, data)


def explained_variance_fn(x_test, y_test, model):
    def explained_variance(_y_true, _y_pred):
        y_pred = model.predict(x_test)
        return backend.variable(sklearn.metrics.explained_variance_score(y_test, y_pred))
    return explained_variance
def r2_fn(x_test, y_test, model):
    def r2(_y_true, _y_pred):
        y_pred = model.predict(x_test)
        return backend.variable(sklearn.metrics.r2_score(y_test, y_pred))
    return r2

data, snp_ids, sample_ids, n_gts = load_snps()
print('n_gts:', n_gts)
data, sample_ids = convert_snps_to_np(data, snp_ids, sample_ids, n_gts)
y, data = get_phenotypes(sample_ids, data, n_gts)
print('Loaded phenotypes')
print('data', data.shape)
print('y', len(y))
n_snps, n_samples, d, input_vector_size = data.shape


# remove nan values
missing_phenos = np.argwhere(np.isnan(y))
print(len(missing_phenos), "missing phenotypes")
#for idx in missing_phenos:
#    data = np.delete(data, idx[0], 0)
#y = y[~np.isnan(y)]

if args.model_type == 'classification':
    class_weights = sklearn.utils.class_weight.compute_class_weight('balanced', np.unique(y), y)

x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(
    data, y, test_size=0.1)

print("x shape:", x_train.shape)

model = Sequential()
model.add(Conv2D(64, (5,1), data_format='channels_last', input_shape=(n_samples, d, input_vector_size), activation='relu'))
model.add(Conv2D(64, (5,1), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 1)))
model.add(Dropout(0.25))

model.add(Conv2D(128, (3, 1), activation='relu'))
model.add(Conv2D(128, (3, 1), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 1)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu', kernel_regularizer=regularizers.l1(0.5)))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu', kernel_regularizer=regularizers.l1(0.5)))
model.add(Dropout(0.5))
model.add(Dense(1, activation='linear' if args.model_type == 'regression' else 'sigmoid'))
if args.model_type == 'regression':
    model.compile(loss='mse',
                  optimizer='adam',
                  metrics=['accuracy'])
else:
    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])
model.summary()
plot_model(model, to_file='arch.png')

kwargs = {'class_weight': class_weights} if args.model_type == 'classification' else {}
print(kwargs)
print(model.fit, callbacks.TerminateOnNaN, callbacks.TensorBoard, callbacks.EarlyStopping)
model.fit(x_train, y_train, epochs=200, batch_size=128, validation_data=(x_test, y_test), callbacks=[
    callbacks.TerminateOnNaN(),
    callbacks.TensorBoard(histogram_freq=1, write_grads=True,
                          log_dir='./logs/%s.%s.%s' % (start_timestamp, args.phenotype, args.data_type)),
    
    callbacks.EarlyStopping('val_loss', min_delta=1, patience=5, )
], **kwargs)

if args.model_type == 'regression':
    y_pred = model.predict(x_train)
    print('Training')
    print('Explained variance:', sklearn.metrics.explained_variance_score(y_train, y_pred))
    print('R2:', sklearn.metrics.r2_score(y_train, y_pred))

    y_pred = model.predict(x_test)
    print('Validation')
    print('Explained variance:', sklearn.metrics.explained_variance_score(y_test, y_pred))
    print('R2:', sklearn.metrics.r2_score(y_test, y_pred))

    filename = "%s.csv" % datetime.datetime.now()
    print("Writing to %s" % filename)
    twofold = [pred <= y*2.0 and pred >= y/2.0 for pred, y in zip(y_pred, y_test)]
    print((type(y_test), type(y_pred)))
    df = pd.DataFrame({
        'y': list(y_test),
        'prediction': list(y_pred),
        '2-fold': twofold,
        })
    with open(filename, 'w') as f:
        f.write(df.to_csv(index=False))


print(model.metrics_names)

if args.model_type == 'classification':
    for x_arr, y_arr in zip([x_train, x_test], [y_train, y_test]):
        print('*'*80, 'training evalutaion')
        print('idx, prediction, actual')
        print(x_arr.shape, y_arr.shape)
        train_predict = model.predict_proba(x_arr)
        for i, pred in enumerate(train_predict):
            print( i, ',', pred, ',', y_arr[i])
        print(','.join(map(str,train_predict)))


model.save('%s.%s.%s.%s.h5' % (args.snp_file, args.phenotype, args.model_type, args.data_type))

# Without FC layers -> 73.85% accuracy
