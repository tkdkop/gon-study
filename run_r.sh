#!/bin/bash
set -e

for phen in {cefixime,ciprofloxacin,azithromycin,penicillin,tetracycline}
do
    mkdir -p rrblup-results

    ./rrblup.R $phen > rrblup-results/${phen}.out
done
