from util import load_from_saved
import pandas as pd
import collections
import vcf
import numpy as np

def load_snps(snp_file, data_type=None):
    data = []
    sample_ids = []
    snp_ids = []
    max_alt = 0
    with open(snp_file, 'r') as f:
        vcf_reader = vcf.Reader(f)
        for record in iter(vcf_reader):
        #for record in itertools.islice(vcf_reader, 100,105):
            _data = collections.defaultdict(list)
            snp_ids.append((record.CHROM, record.POS))
            save_sample_name = False
            if not sample_ids:
                # save all sample id's from the first SNP record
                save_sample_name = True

            for sample in record.samples:
            #for sample in [record.samples[0]]:
                #print(sample, sample.data, sample.data.GT)
                if save_sample_name:
                    sample_ids.append(sample.sample)

                gl = sample.data.GL

                if gl is not None:
                    _data[sample.sample] = gl
                    if len(gl) > max_alt:
                        print(sample)
                        print(sample.data)
                        print(sample.data.GL)
                        print(sample.gt_bases)
                        print(sample.gt_type)

                        max_alt = len(gl)
            data.append(_data)
    print(len(sample_ids), len(snp_ids))
    return data, snp_ids, sample_ids, max_alt


def convert_snps_to_np(snp_dict, snp_ids, sample_ids, n_gts, data_type):
    # data is of the form:
    # list:SNP(dict:Sample,call)
    # [ { A:A1, B:B1, C:C1 }
    #   { A:A2, B:B2, C:C2 }
    #   { A:A3, B:B3, C:C3 } ]
    # it needs to be in the form:
    # [ [ A1 A2 A3 ]
    #   [ B1 B2 B3 ]
    #   [ C1 C2 C3 ] ]
    # Where letters are the sample identifiers, and numbers are the snp identifiers
    # Final size should be (676, 7, 41502, 1) after 1-hot encoding
    missing_calls = 0
    data = np.zeros(shape=(len(sample_ids), n_gts, len(snp_ids), 1))
    print(data.shape)
    for i, sample_id in enumerate(sample_ids):
        for j, snp_id in enumerate(snp_ids):
            gl = snp_dict[j][sample_id]
            if data_type == 'uncertain':
                gl = np.array([float(10**gt) for gt in gl])
                # You lose this to precision anyways
                #one = np.where(gl == 1)
                #gl[one] = 1.0 - (sum(gl) - 1)
            else:
                gl = np.array(gl)
                gl = (gl == 0) * 1
            for k,gt in enumerate(gl):
                data[i][k][j][0] = gt

    print('missed', missing_calls, missing_calls/(len(snp_ids)*len(sample_ids))*100, '%')
    print(data.shape)
    return data, sample_ids

def get_phenotypes(sample_ids, phen_file, phenotype=None, model_type=None, gen_data=None, n_gts=None):
    # Must match sample_ids ordering
    # Cefixime resistance = MIC > 0.125mg/L
    pheno_cols = {
        'cefixime': 'Phenotype',
        'penicillin': 'Phenotype.1',
        'azithromycin': 'Phenotype.2',
        'ciprofloxacin': 'Phenotype.3',
        'tetracycline': 'Phenotype.4',
    }
    pheno_vals = {
        'cefixime': 0.125,
        'azithromycin': 1,
        'ciprofloxacin': 0.06,
        'tetracycline': 0.25,
        'penicillin': 0.06,

    }
    df = pd.read_excel(phen_file, header=1)
    data = {}
    for sample, mic in zip(df['NCBI identifier'], df[pheno_cols[phenotype]]):
        sample_with_prefix = 'US-' + sample if sample.startswith('E') else 'UK-' + sample
        if mic is pd.np.nan:
            mic = 0
        data[sample_with_prefix] = mic
    gen_data = np.reshape(gen_data, (676, -1,1,n_gts))
    if model_type == 'regression':
        return np.array([data[sample_id]  for sample_id in sample_ids]), gen_data
    else:
        return np.array([data[sample_id] > pheno_vals[phenotype] for sample_id in sample_ids]), gen_data



